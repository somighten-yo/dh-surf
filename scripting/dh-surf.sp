/**
*
* Plugin for DH surf servers
* By: Somighten
* https://bitbucket.org/somighten-yo/dh-surf/src/master/
*
* Allows client to get DH related info
* Version 0.1
*
**/

#include <sourcemod>
#include <sdktools>
#include <cstrike>

//Menu dhinfo
Handle menuhandle;

public Plugin myinfo = {
	name = "dh-surf",
	author = "Somighten",
	description = "Allows client to get DH reltated info",
	version = "0.1",
	url = "https://bitbucket.org/somighten-yo/dh-surf/src/master/"
	};

public void OnPluginStart()
{

	RegConsoleCmd( "sm_info", info);
}

public void OnClientPutInServer(client)
{
	menuhandle = CreateMenu(MenuCallBack);
	DhMenu(client);
}

public Action info(int client, int args)
{
	DhMenu(client);
	return Plugin_Handled;
}

public Action DhMenu(int client)
{
	Handle:menuhandle = CreateMenu(MenuCallBack);
	SetMenuTitle(menuhandle, "Welcome to DH Surf");

	AddMenuItem(menuhandle, "donate", "Donations help keep the lights on");
	AddMenuItem(menuhandle, "discord", "Join us on Discord");
	AddMenuItem(menuhandle, "servers", "Other DH game servers");	
	AddMenuItem(menuhandle, "plugins", "!ws, !profile, !rank, !top");
	AddMenuItem(menuhandle, "info", "!info to show this message again");
	AddMenuItem(menuhandle, "contrib", "Contribute in other ways to get plugins");		
	AddMenuItem(menuhandle, "donor", "More plugins for Donors");
	
	SetMenuPagination(menuhandle, 7);
	SetMenuExitButton(menuhandle, true);
	DisplayMenu(menuhandle, client, 250);
	
}

public MenuCallBack(Handle:menuhandle, MenuAction:action, client, Position)
{
	if(action == MenuAction_Select)
	{
		decl String:Item[20];
		/* save menu item in string Item */
		GetMenuItem(menuhandle, Position, Item, sizeof(Item));

		if(StrEqual(Item, "donate") || StrEqual(Item, "donor") && (IsClientConnected(client) && IsClientInGame(client)))
		{
			PrintToChat(client, "*************************** ");
			PrintToChat(client, "Donate with Bitcoin or Patreon");
			PrintToChat(client, "Patreon: https://www.patreon.com/dhretake");
			PrintToChat(client, "bitcoin:3HKUGe6978n2q2MAD7tJ8K47vWdzsZ78Sq");
			PrintToChat(client, "Donors get access to !knife and !gloves in Surf.");
			PrintToChat(client, "Donors also get !knife, !gloves, !music, !profile, !coin in the Retake server");
			PrintToChat(client, "Contact DanielRow@protonmail.com");
			PrintToChat(client, "*************************** ");			
		}
		if(StrEqual(Item, "discord") && (IsClientConnected(client) && IsClientInGame(client)))
		{
			PrintToChat(client, "*************************** ");
			PrintToChat(client, "Join us on Discord");
			PrintToChat(client, "https://discord.gg/8quydCg");
			PrintToChat(client, "Donors get special roles on Discord");
			PrintToChat(client, "Contact DanielRow@protonmail.com");
			PrintToChat(client, "*************************** ");
		}

		if(StrEqual(Item, "servers") && (IsClientConnected(client) && IsClientInGame(client)))
		{
			PrintToChat(client, "*************************** ");
			PrintToChat(client, "Main Retake server: ec1.determinedherbivore.com");
			PrintToChat(client, "Retake with extra maps: mw1.determinedherbivore.com");
			PrintToChat(client, "Gmod server(beta): ec3.determinedherbivore.com");
			PrintToChat(client, "Contact DanielRow@protonmail.com");
			PrintToChat(client, "*************************** ");
		}
		
		if(StrEqual(Item, "contrib") && (IsClientConnected(client) && IsClientInGame(client)))
		{
			PrintToChat(client, "*************************** ");
			PrintToChat(client, "Always things to be done");
			PrintToChat(client, "Good at map making? Image editing? Surf zones?");
			PrintToChat(client, "Community help for plugin access is always welcome");
			PrintToChat(client, "Contact DanielRow@protonmail.com");
			PrintToChat(client, "*************************** ");
		}
		
		if(StrEqual(Item, "plugins") && (IsClientConnected(client) && IsClientInGame(client)))
		{
			PrintToChat(client, "*************************** ");
			PrintToChat(client, "!ws, !profile, !rank, !top !r");
			PrintToChat(client, "Just type the commands into chat");
			PrintToChat(client, "Donors get access to !knife !gloves, and more in Retake");
			PrintToChat(client, "Contact DanielRow@protonmail.com");
			PrintToChat(client, "*************************** ");
		}
		if(StrEqual(Item, "info") && (IsClientConnected(client) && IsClientInGame(client)))
		{
			PrintToChat(client, "*************************** ");
			PrintToChat(client, "!info for DH information");
			PrintToChat(client, "Just type the command into chat");
			PrintToChat(client, "Patreon and public blogs at patreon.com/dhretake");
			PrintToChat(client, "Contact DanielRow@protonmail.com");
			PrintToChat(client, "*************************** ");
		}
		
		
	}
	else if(action == MenuAction_End)
	{
		CloseHandle(menuhandle);
	}
}

